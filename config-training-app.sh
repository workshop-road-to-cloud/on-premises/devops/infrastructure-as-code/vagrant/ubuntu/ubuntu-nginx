#!/bin/bash
echo "Start script execution---->>config-training-app.sh<----"

sudo -s

echo "create bk of original nginx"
cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default-original.bk

echo "create a new  default-app for training-app"
echo 'server {
        listen 80 default_server;
        listen [::]:80 default_server;
        root /var/www/html;
        server_name _;
        location / {
                index index.html index.htm;
                try_files $uri $uri/ /index.html;
        }
} ' > /etc/nginx/sites-available/default-app

echo "create configuration of build-app"

cd /opt/

if [ ! -d "/opt/build-app/" ]; then
  echo "/opt/build-app/ does not exist."
  mkdir /opt/build-app/
fi

cd /opt/build-app/

echo 'echo "Start execution of start-build-app.sh"

if [ -d "/var/www/html/" ]; then
  echo "/var/www/html/ does exist."
  rm -rf /var/www/html/
fi

if [ -d "training-app" ]; then
  echo "training-app does exist."
  rm -rf training-app
fi
echo "clone repository"
git clone -b main https://gitlab.com/workshop-road-to-cloud/workspace/source-code/training-app.git
cd training-app/web/training-app-web/
echo "build and install application"
npm install
ng build --configuration=production
echo "deploy application"
cd dist/
cp -rf training-app-web/ /var/www/
mv /var/www/training-app-web/ /var/www/html/

echo "End execution of start-build-app.sh"' > /opt/build-app/start-build-app.sh

chmod +777 /opt/build-app/start-build-app.sh

rm /etc/nginx/sites-available/default
cp /etc/nginx/sites-available/default-app /etc/nginx/sites-available/default

cd /opt/build-app/
./start-build-app.sh

systemctl restart nginx

echo "End script execution---->>config-training-app.sh<<----"